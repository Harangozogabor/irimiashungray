<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Irimias Hungary Plusz KFT</title>
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
      <script type="text/javascript" src="js/bootstrap.js"></script>
      <script type="text/javascript" src="js/main.js"></script>
      <script type="text/javascript" src="js/scroll.js"></script>
      <script src="./AlertifyJS-master/build/alertify.min.js"></script>
      <link rel="stylesheet" href="./AlertifyJS-master/build/css/alertify.min.css" />
      <link rel="stylesheet" href="./AlertifyJS-master/build/css/themes/default.min.css" />
  </head>
  <body>
 <div id="loader-wrapper">
      <div id="centered" class = "centered">
          <div class = "blob-1"></div>
          <div class = "blob-2"></div>
      </div>

      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>

  </div>
    <nav id="menu" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="img/logo.png" width="200" height="50" alt="" ></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home"  class="page-scroll">Home</a></li>
                    <li><a href="#about" class="page-scroll">Rólunk</a></li>
                    <li><a href="#apply" class="page-scroll">Jelentkezés</a></li>
                    <li><a href="#why" class="page-scroll">Miért mi?</a></li>
                    <li><a href="#proud" class="page-scroll">Amire büszkék vagyunk</a></li>
                    <li><a href="#works" class="page-scroll">Referenciák</a></li>
                    <li><a href="#contact" class="page-scroll">Kapcsolat</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


    <!-- Home Page
    ==========================================-->
    <div id="home" class="text-center">
        <div class="overlay">
            <div class="content">
                <h1><strong><p class="color">Irimias Hungary Plusz KFT</p></strong></h1>
                <p class="lead"><strong>2 éve</strong>  foglalkozunk bérmunkázással</p>
                <a href="#about" class="fa fa-angle-down page-scroll"></a>
            </div>
        </div>
    </div>

    <!-- About Us Page
    ==========================================-->
    <div id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-text">
                        <div class="section-title center">
                            <h2>Néhány szó <strong>rólunk</strong></h2>
                            <div class="line">
                                <hr>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <p class="intro">A cég 100% magyar tulajdonban van. Az IRIMIAS Hungary Plusz Kft. magyarországi bejegyzésű cég, mely az Job Service Hungary Kft.-vel együttműködve dolgozik. A cég alapítók szándéka: Ausztriában, Németországban, Belgiumban, Hollandiában és az egész Európai Unió területén alapvetően magyar munkaerő alkalmazásával, de határon túli magyarokat is alkalmazva betanított munkákra, targoncás munkákra.
                            Tevékenységi körünkbe tartozik a bérmunka valamint a fémipari bérmunka. .Szakképzett, referenciákkal rendelkező bérmunkásokat, targoncásokat foglalkoztatunk.
                            Jelenleg 50 fő áll rendelkezésünkre azonnali munkakezdésre.
                            Amennyiben felkeltettük érdeklődését, várjuk jelentkezésüket email-ben a jelentkezési lap kitöltésével.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="img/us.jpg" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <!-- Team Page
    ==========================================-->
    <div id="apply" class="text-center">
        <div class="overlay">
            <div class="container">
                <div class="section-title center">
                    <h2><strong>Rád</strong> van szükségünk</h2>
                    <div class="line">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">

                    </div>
                    <div class="col-sm-8">
                        <h4 class="form">
                            Töltsd ki az adatlapunk és add be itt a jelentkezésed!
                        </h4>
                        <!--<form method="post" action="form_proccess.php" enctype="multipart/form-data">
                            <label>
                                <span>Your Name</span>
                                <input type="text" value="Jon Snow" name="s_name" />
                            </label>
                            <label>
                                <span>Your Email</span>
                                <input type="email" value=""  name="s_email" />
                            </label>
                            <label>
                                <span>Message</span>
                                <textarea name="s_message"></textarea>
                            </label>
                            <label>
                                <span>Attachments</span>
                                <!-- File input fields, you can add as many as required
                                <input type="file" name="file[]" />
                                <input type="file" name="file[]" />
                                <input type="file" name="file[]" />
                                <!-- OR -->
                                <!-- <input type="file" name="file[]" multiple="multiple" />
                            </label>
                            <label>
                                <input type="submit" value="Send" />
                            </label>
                        </form>-->
                        <?php include('form_proccess.php'); ?>
                        <form method="post" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="nev">
                                        A teljes neved:</label>
                                    <input type="text" class="form-control" id="name" name="s_name" required>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="email">
                                        Email címed:</label>
                                    <input type="email" class="form-control" id="email" name="s_email" required>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="oneletrajz">
                                        Önéletrajz</label>
                                    <input type="file" class="form-control" name="file[]"  multiple="multiple">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="szemelyi">
                                        Személyiazonosító igazolvány  másolata:</label>
                                    <input type="file" class="form-control" name="file[]"  multiple="multiple">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="adoszam">
                                        Adószám:</label>
                                    <input type="text" class="form-control"  name="taxNumber" >
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="lakcím">
                                        Lakcímkártya másolata:</label>
                                    <input type="file" class="form-control" name="file[]"  multiple="multiple">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="tajszam">
                                        Tajkárya száma: </label>
                                    <input type="text" class="form-control"  name="tajNumber" >
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="lakcím">
                                        Tb kártya másolata:</label>
                                    <input type="file" class="form-control" name="file[]"  multiple="multiple">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6  form-group">
                                    <label for="adatvédelem">
                                        Elolvastam és elfogadom az <a href="adat.pdf">adatvédelmi nyilatkozatot<a/></label>
                                    <input type="checkbox"  onchange="document.getElementById('send').disabled = !this.checked;" />
                               </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <button type="submit" value="Send" disabled id="send" class="btn tf-btn btn-default pull-right">Küldés</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>

    <!-- Services Section
    ==========================================-->
    <div id="why" class="text-center">
        <div class="container">
            <div class="section-title center">
                <h2>Miért válassz <strong>minket ?</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
             </div>
            <div class="space"></div>
            <div class="row" id="contents">
                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-building"></i>
                    <h4><strong>Ideiglenes lakcím</strong></h4>
                    <p>Minden nálunk dolgozó ideiglenes lakcímkártyát kap az adott országban, így ugyanolyan jogai vannak mint az ott élőknek</p>
                </div>
                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-money-bill"></i>
                    <h4><strong>Magas bérezés</strong></h4>
                    <p>A piacon lévő bérmunkázással foglalkozó cégek közül, mi tudjuk biztosítani az egyik legkiemelkedőbb fizetést.</p>
                </div>
                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-dollar-sign"></i>
                    <h4><strong>Fizetési előleg</strong></h4>
                    <p>Minden nálunk dolgozó a hét elején fizetési előleget kap.</p>
                </div>
                <div class="col-md-3 col-sm-6 service">
                    <i class="fa fa-car"></i>
                    <h4><strong>Szállás és utazás biztosítása</strong></h4>
                    <p>Cégünk minden dolgozónak biztosítja az utazást és a szállást. </p>
                </div>


            </div>
        </div>
    </div>
    <!-- Testimonials Section


    <!-- Portfolio Section
    ==========================================-->
    <div id="works">
        <div class="container"> <!-- Container -->
            <div class="section-title text-center center">
                <h2 class="title">Vessen egy pillantást a <strong>referenciáinkra</strong></h2>
                <div class="line">
                    <hr>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="space"></div>

            <div id="lightbox" class="row">

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/01.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/02.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/03.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/04.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/05.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/06.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/07.jpg" class="img-responsive" alt="...">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="portfolio-item">
                        <div class="hover-bg">
                            <a href="#">
                                <div class="hover-text">
                                    <h4>Cím</h4>
                                    <small>rövid leírás</small>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="img/portfolio/08.jpg" class="img-responsive" >
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Contact Section
    ==========================================-->
    <div id="contact" class="text-center">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="section-title center">
                        <h2>Kérdezzen <strong>tőlünk</strong></h2>
                        <div class="line">
                            <hr>
                        </div>
                        <div class="clearfix"></div>
                        <small>Írjon nekünk emailt és mi 24 órán belül válaszolunk.</small>
                    </div>
                    <?php include('form_proccess2.php'); ?>
                    <div class="col-md-12">

                    </div>
                    <form id="contact"  action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Email cím</label>
                                    <input type="email" class="form-control" id="Email1" name="email" placeholder="Kérem adja meg az email címét" value="<?= $email ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Tárgy</label>
                                    <input type="text" class="form-control" id="targy1" name="what" placeholder="Tárgy" value="<?= $what ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Üzenet</label>
                            <textarea class="form-control" rows="3" name="message"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-sm-6  form-group">
                                <label for="adatvédelem">
                                    Elolvastam és elfogadom az <a href="adat.pdf">adatvédelmi nyilatkozatot<a/>
                                <input type="checkbox"  onchange="document.getElementById('submit').disabled = !this.checked;" />
                            </div>
                        </div>
                        <button type="submit" disabled id="submit" class="btn tf-btn btn-default">Küldés</button>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-8 offset 2">
                    <h4>Vagy jöjjön be hozzánk személyesen,<br> 5600 Békéscsaba Kazinczy utca 4.</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-8 offset 2">
                    <h4>Bátran keressen minket telefonon is<br> Horváth Viktor, Területi vezető <br>+3620/9992999 </h4>
                </div>
            </div>

        </div>
    </div>
 <div id="cookieConsent">
     <div id="closeCookieConsent">x</div>
     Mint minden weboldal, a irimiashungaryplusz.hu is használ cookie-kat, hogy kellemesebb felhasználói élményben legyen részed, amikor az oldalunkon jársz. Az “Értem” gomb lenyomásával hozzájárulásodat adod, hogy elfogadod őket. További tudnivalók a cookie-król     <a href="https://hu.wikipedia.org/wiki/HTTP-s%C3%BCti" target="_blank">Mi az a cookie?</a>. <a class="cookieConsentOK">Értem</a>
 </div>
    </body>
  <script>

      $(window).on('load', function(){
          $('body').addClass('loaded');
          document.getElementById('centered').style.opacity=0;
      });
      $(window).bind('scroll', function() {
          if(document.getElementById('why').offsetTop  <=$(window).scrollTop()+((window).screen.height/2) ){

              document.getElementById('contents').style.opacity = 1;
          }
      });

  </script>
</html>